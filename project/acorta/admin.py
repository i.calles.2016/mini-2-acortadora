from django.contrib import admin
from .models import DataBase, Counter
admin.site.register(DataBase)
admin.site.register(Counter)

# Register your models here.
