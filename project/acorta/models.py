from django.db import models


# Create your models here.
class DataBase(models.Model):
    url = models.TextField()
    url_shorten = models.TextField()


class Counter(models.Model):
    value = models.IntegerField(default=0)
