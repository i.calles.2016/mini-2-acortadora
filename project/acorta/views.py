from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseNotFound
import urllib.parse
from .models import DataBase, Counter


def increment_counter():

    if not Counter.objects.exists():
        Counter.objects.create(value=0)

    counter = Counter.objects.get(id=1)
    counter.value = counter.value + 1
    counter.save()

    return counter.value


def short_count(request):

    counter_resource = increment_counter()

    myport = request.META['SERVER_PORT']
    short = "http://localhost:" + str(myport) + "/" + str(counter_resource)

    return short


def urlPrefix(url):
    if not url.startswith('https://') and not url.startswith('http://'):
        prefix = "https://"
        url = "".join([prefix, url])

    return urllib.parse.unquote(url)


def post(request, dataBase):
    if len(request.body) == 0 or request.body.decode('utf-8') == 'url=&short=':
        return render(request, 'acorta/body_empty.html', {'url_list': dataBase})
    else:
        url = urlPrefix(request.body.decode('utf-8').split('&')[0].split('=')[1])
        short = urllib.parse.unquote(request.body.decode('utf-8').split('&')[1])
        if short == 'short=':
            try:
                db = DataBase.objects.get(url=url)
            except DataBase.DoesNotExist:
                db = DataBase(url=url)

            short = short_count(request)
            db.url_shorten = short
            db.save()

            return render(request, 'acorta/update.html', {'url_list': dataBase})

        else:
            short = short.split('=')[1]
            myport = request.META['SERVER_PORT']
            try:
                DataBase.objects.get(url_shorten="http://localhost:" + str(myport) + "/" + str(short))
                return render(request, 'acorta/url_shorten_repeated.html', {'url_list': dataBase})
            except DataBase.DoesNotExist:
                db = DataBase(url=url)
                short = "http://localhost" + str(myport) + "/" + str(short)
                db.url_shorten = short
                db.save()
                return render(request, 'acorta/update.html', {'url_list': dataBase})


def get(request, dataBase):
    return render(request, 'acorta/index.html', {'url_list': dataBase})


@csrf_exempt
def index(request):
    dataBase = DataBase.objects.all()
    if request.method == 'GET':
        response = get(request, dataBase)

    if request.method == 'POST':
        response = post(request, dataBase)

    return response


def redirect(request, resource):
    dataBase = DataBase.objects.all()
    myport = request.META['SERVER_PORT']
    try:
        db = DataBase.objects.get(url_shorten="http://localhost:" + str(myport) + "/" + resource)
        return render(request, 'redirect/redirect.html', {'url': db.url})
    except DataBase.DoesNotExist:
        return render(request, 'redirect/resource_not_exist.html', {'url_list': dataBase})
